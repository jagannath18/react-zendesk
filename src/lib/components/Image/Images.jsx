/* eslint-disable no-unused-vars */
import React from 'react'

const Image = props => {
  const { image, altText, height, width } = props.imageSet
  return (
    <picture>
      {/* <source srcSet={webp} type="image/webp" />
      <source srcSet={jpg} type="image/jpeg" /> */}
      <img src={image} alt={altText || "Alt Text!"} height={height || 50} width={width || 50} />
    </picture>
  )
}

export default Image
