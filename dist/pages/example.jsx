// Dependency Imports
import React from 'react'
import { FormattedMessage } from "react-intl";
import PropTypes from "prop-types";

// Custom Imports
import '../../static/scss/App.scss'
import { APP_NAME, MAIN_LOGO, WEBP_DUMMY_1 } from '../../config';
import CustomLink from '../components/Link/Link.jsx';
import MainHeader from '../../containers/headers/mainHeader.jsx';
import Image from '../components/Image/Images.jsx';

class ExampleIndex extends React.Component {
  render() {
    const { lang, dispatch } = this.props;
    
    return (
      <>
        {/* Header Section */}
        <div className="App">
          <MainHeader language={{ lang: lang }} dispatch={dispatch}>
            <h3 className="headerMainTitle">
              <FormattedMessage tagName="span" id="message.welcome" />
              <span className='ml-2'>to Create-React-App Boilerplate Guide, by {APP_NAME}.</span></h3>
            <p>
              Edit <code>src/App.js</code> and save to reload.
            </p>
            <p>or</p>

            <p className="App-link"><CustomLink url="/search">Click here to visit Search Page</CustomLink></p>
          </MainHeader>
        </div>

        {/* Body Section */}
        <div className="mainBodyPart">
          <Image imageSet={{ png: MAIN_LOGO, webp: WEBP_DUMMY_1 }} />
        </div>
      </>
    )
  }
}

// Create validations for Props, required or type etc.
ExampleIndex.propTypes = {
  dispatch: PropTypes.any.isRequired,
  lang: PropTypes.any.isRequired
};

export default ExampleIndex;
