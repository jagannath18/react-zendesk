
import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import { CANCEL_IMAGE, DEFAULT_GALLERY, PRIORITY_LEVEL_VALUE } from '../../config';
import "../../static/scss/writeus/writeus.scss";

function getModalStyle(props) {
    const top = 50;
    const left = 50;
    return {
        width: props,
        top: `${top}%`,
        left: `${left}%`,
        borderRadius: "8px",
        transform: `translate(-${top}%, -${left}%)`,
    };
}

const styles = (theme) => ({
    paper: {
        position: "absolute",
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        outline: "none",
    },
});

class Write_US_Modal extends React.Component {
    state = {
        priority_level_names: Object.keys(PRIORITY_LEVEL_VALUE),
        image: ''
    }

    selectPriority = (event) => {
        const event_name = event ? event.toUpperCase() : event;
        if (this.state.priority_level_names.includes(event_name)) this.props.selectPriority(event_name);
    }

    selectImage = (event) => {
        if (event.target.files && event.target.files[0]) {
            this.setState({ image: URL.createObjectURL(event.target.files[0]) });
        }
    }

    render() {
        const { classes } = this.props;
        return (
            <div>
                <Modal aria-labelledby="simple-modal-title" aria-describedby="simple-modal-description" open={this.props.isOpen} onClose={this.props.toggle}>
                    <div style={getModalStyle(this.props.width)} className={classes.paper}>
                        <div className="modal__sec">
                            <div className="col-12">
                                <h6 className="mb-0 text-center sub__hdr">Write to us</h6>
                                <img src={CANCEL_IMAGE} width={16} className="close__img" alt="close-modal" onClick={this.props.toggle} />
                            </div>
                            <div className="col-12 mb-3">
                                <p className="sub__hdr">Priority</p>
                                <div className="form-row align-items-center">
                                    <div className="col-auto">
                                        <button onClick={(e) => this.selectPriority(e.target.innerText)} className="btn-high active">Urgent</button>
                                    </div>
                                    <div className="col-auto">
                                        <button onClick={(e) => this.selectPriority(e.target.innerText)} className="btn-Medium">High</button>
                                    </div>
                                    <div className="col-auto">
                                        <button onClick={(e) => this.selectPriority(e.target.innerText)} className="btn-Average">Normal</button>
                                    </div>
                                    <div className="col-auto">
                                        <button onClick={(e) => this.selectPriority(e.target.innerText)} className="btn-Low">Low</button>
                                    </div>
                                </div>
                            </div>
                            <div className="border-top border-bottom py-3">
                                <div className="col-12">
                                    <p className="sub__hdr">Add your issue</p>
                                    <textarea cols={30} rows={3} className="form-control inpt__el" onChange={this.props.queryHandler} placeholder="Explain your issue" defaultValue={""} />
                                </div>
                            </div>
                            <div className="col-12 py-3">
                                <p className="sub__hdr mb-0">Add image</p>
                                <div className="txt__desc mb-3">Please upload the image (Max 5 images)</div>
                                <div className="mb-3">
                                    <label htmlFor="upload-image">
                                        <img src={DEFAULT_GALLERY} htmlFor="upload-image" width={60} size={20} height={60} className="def__gall__img" alt="default-gallery" />
                                    </label>
                                    <input type="file" id="upload-image" style={{ display: "none" }} onChange={this.selectImage} />
                                    {this.state.image && <img width={60} size={20} height={60} src={this.state.image} alt="preview-image" />}
                                </div>
                                <button className="add__address" onClick={this.props.submitQuery}>Submit</button>
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }
}

Write_US_Modal.propTypes = {
    classes: PropTypes.object.isRequired,
};

// We need an intermediary variable for handling the recursive nesting.
const _Write_US_Modal = withStyles(styles)(Write_US_Modal);

export default _Write_US_Modal;