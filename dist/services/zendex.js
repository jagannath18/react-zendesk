import { get, post, putWithToken } from '../request';
export const CREATE_NEW_ISSUE = async (subject, body, status = 'new', priority, type = 'question', requester_id) => {
  const data = {
    subject,
    body,
    status,
    priority,
    type,
    requester_id
  };
  if (data && data.requester_id && data.subject && data.priority) return await post('/ticket', data);
};
export const GET_ALL_TICKETS = email => {
  if (email) return get(`/user/ticket?emailId=${email}`);
};
export const GET_TICKET_HISTORY = id => {
  if (id) return get(`/ticket/history?id=${id}`);
};
export const REPLY_TO_COMMENT = (id, body, author_id) => {
  const data = {
    id,
    body,
    author_id
  };
  if (id && body && author_id) return putWithToken('/ticket/comments', data);
};