module.exports = {
  'env': {
    'production': {
      'presets': [],
      'plugins': [
        'transform-export-extensions', 
        "@babel/plugin-syntax-jsx",
        "@babel/plugin-proposal-optional-chaining",
        [
          '@babel/plugin-proposal-class-properties',
          {
            'loose': true
          }
        ],
      ],
      'only': [
        './**/*.js',
        'node_modules/jest-runtime'
      ]
    }
  }
}
